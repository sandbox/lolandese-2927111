Makes that non multilingual nodes get the neutral language (und: undefined)
instead of the default language as a solution for monolingual sites that have
the core Locale module enabled. This is needed for example to have site content
in one language and the administration interface in English or the other
language depending on user preference.

If you have a site with:

- only one non-English content language (set as default language)
- users that use either an English or non-English Drupal backend UI

the users with an English UI do not see Views or Entity References content in
the non-English language. So it differs from what anonymous visitors would see.
In the above case you would have the Drupal core module Locale enabled and
Content translation disabled.

If you have only one content language probably all content types have the
Multilingual support set to Disabled under Publishing options. In that case
default Drupal core behavior is to set the language to the default language
(non-English). This module changes that to be the neutral language (und:
undefined) so also users with the English UI still see it if embedded on other
pages as a View or Entity Reference.


Switch the backend UI to English for users that have it set as their language
=============================================================================

- Go to admin/config/regional/language
- Choose the Detection and Selection tab at
  admin/config/regional/language/configure
- Activate the "User detection" and drag it to the top of the detection methods.


Switch default language to for example NL and import latest .po file
====================================================================

Download the translation file for Dutch from
http://ftp.drupal.org/files/translations/7.x/drupal/drupal-7.56.nl.po . Check if
the version number is the one of the Drupal version you are using.

Go to admin/config/regional/translate/import and import the above file. It might
take a while.

Go to admin/config/regional/language and set the default language to Dutch.


Example update hook to change existing language fields
======================================================

/**
 * Perform the queries to switch DB all language fields from NL to UND.
 */
function updatedb_update_7001(&$sandbox) {
  $table_list = array(
    'node',
  );
  foreach ($table_list as $table) {
    db_update($table)
      ->fields(array(
        'language' => 'und',
      ))
      ->condition('language', 'nl', '=')
      ->execute();
  }
}


Alternatively use https://www.drupal.org/project/admin_language
