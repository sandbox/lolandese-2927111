<?php

/**
 * @file
 * Neutral language instead of default module.
 */

/**
 * Implements hook_help().
 */
function language_und_help($path, $arg) {
  switch ($path) {
    case 'admin/help#language_und':
      // Return a line-break version of the README.txt.
      return _filter_autop(file_get_contents(dirname(__FILE__) . '/README.txt'));
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function language_und_form_locale_languages_overview_form_alter(&$form, &$form_state, $form_id) {
  $form['language_und'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set new or edited content language to neutral (und) instead of the default language.'),
    '#default_value' => variable_get('language_und', 1),
    '#description' => t('Only applicable if the content type has Multilingual support Disabled.'),
  );
  $form['#submit'][] = 'language_und_form_locale_languages_overview_form_submit';
}

/**
 * Submit form data.
 */
function language_und_form_locale_languages_overview_form_submit($form, &$form_state) {
  variable_set('language_und', $form_state['values']['language_und']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function language_und_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (variable_get('language_und', 1)) {
    $form['#submit'][] = 'language_und_form_node_form_submit';
  }
}

/**
 * Submit form data.
 */
function language_und_form_node_form_submit($form, &$form_state) {
  if (!isset($form_state['input']['language'])) {
    $form_state['values']['language'] = 'und';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function language_und_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $form['workflow']['language_content_type']['#description'] = t('Enable multilingual support for this content type. If enabled, a language selection field will be added to the editing form, allowing you to select from one of the <a href="!languages">enabled languages</a>. Depending on <a href="!languages">global settings</a>, if disabled, new posts are saved with no language, also known as neutral or undefined (und). Existing content will be affected by changing this option only if edited.', array('!languages' => url('admin/config/regional/language')));
}
